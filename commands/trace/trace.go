package trace

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jramsay/git-lab/color"
	"gitlab.com/jramsay/git-lab/commands/root"
	"gitlab.com/jramsay/git-lab/git"
	"gitlab.com/jramsay/git-lab/gitlab"
	"gitlab.com/jramsay/git-lab/utils"

	"github.com/alecthomas/kingpin"
	g "github.com/jamesramsay/go-gitlab"
	"github.com/renstrom/fuzzysearch/fuzzy"
)

func init() {
	var (
		cmd           = root.Command("trace", "Display CI job information")
		jobIdentifier = cmd.Arg("job", "Show information about a specific job").String()
		watch         = cmd.Flag("watch", "Watch job trace").Bool()
	)

	cmd.Action(func(_ *kingpin.ParseContext) error {
		localRepo, err := gitlab.LocalRepo()
		utils.Check(err)

		project, err := localRepo.DefaultProject()
		utils.Check(err)

		gl := gitlab.NewClient(project.Host)

		jobId, err := strconv.Atoi(*jobIdentifier)
		if err != nil {

			sha, err := git.Ref("HEAD")
			utils.Check(err)

			branch, err := localRepo.CurrentBranch()
			utils.Check(err)
			ref := branch.ShortName()

			pipeline, err := gitlab.FetchLastPipelineByCommit(gl, project, sha, ref)
			utils.Check(err)

			jobNames := []string{}
			for _, j := range pipeline.Jobs {
				if *jobIdentifier == j.Name {
					jobNames = []string{j.Name}
					jobId = j.Id
					break
				} else if fuzzy.Match(*jobIdentifier, j.Name) {
					jobNames = append(jobNames, j.Name)
					jobId = j.Id
				}
			}

			sort.Strings(jobNames)

			if *jobIdentifier == "" {
				fmt.Printf("Job is a required argument.\n\n")
			}

			if len(jobNames) == 0 && *jobIdentifier != "" {
				fmt.Printf("No jobs matching name: %s\n\n", *jobIdentifier)
				return nil
			} else if len(jobNames) > 1 {
				fmt.Println("Did you mean:")
				for _, name := range jobNames {
					j := pipeline.FindJob(name)
					fmt.Printf("  %s %s\n", color.CommitStatus(j.Status, gitlab.StatusGlyph(j.Status)), j.Name)
				}
				return nil
			}
		}

		if *watch != true {
			trace, _, err := gl.Jobs.GetTraceFile(project.Path(), jobId, nil)
			utils.Check(err)

			io.Copy(os.Stdout, trace)
		} else {
			watchTrace(gl, project, jobId)
		}

		return nil
	})
}

func watchTrace(c *g.Client, project *gitlab.Project, jobId int) {
	isPending := true
	isRunning := true
	cursor := 0
	retryFrequency := 1 * time.Second
	maxRetry := 20
	retries := 0

	for isPending {
		job, _, err := c.Jobs.GetJob(project.Path(), jobId, nil)
		utils.Check(err)

		isPending = (job.Status == "pending")

		if isPending {
			if retries > maxRetry {
				// TODO: error message
				return
			}
			time.Sleep(retryFrequency)
		}
	}

	retries = 0

	for isRunning {
		t, _, err := c.Jobs.GetTraceFile(project.Path(), jobId, nil)
		utils.Check(err)
		if trace, err := ioutil.ReadAll(t); err == nil {
			runes := []rune(string(trace))
			fmt.Print(string(runes[cursor:]))
			cursor = len(runes)

			tail := string(runes[cursor-20:])

			if strings.Contains(tail, "Job succeeded") || strings.Contains(tail, "Job failed") || retries > maxRetry {
				job, _, err := c.Jobs.GetJob(project.Path(), jobId, nil)
				utils.Check(err)

				isRunning = (job.Status == "running")

				// Reset if it's actually still running!
				retries = 0
			}

			if isRunning {
				time.Sleep(retryFrequency)
			}
		} else {
			utils.Check(err)
		}
	}

	return
}
