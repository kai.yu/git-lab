package lint

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/alecthomas/kingpin"

	"gitlab.com/jramsay/git-lab/commands/root"
	"gitlab.com/jramsay/git-lab/gitlab"
)

var (
	lintCmd = root.Command("lint", "Run CI lint against your .gitlab-ci.yml")
	path    = lintCmd.Flag("path", "Path to your .gitlab-ci.yml").Default(".gitlab-ci.yml").String()
)

func init() {
	lintCmd.Action(func(_ *kingpin.ParseContext) error {
		client, err := gitlabClient()
		if err != nil {
			return err
		}

		content, err := ioutil.ReadFile(*path)
		if err != nil {
			return err
		}

		results, _, err := client.Validate.Lint(string(content))
		if err != nil {
			return err
		}

		// Linting returned errors
		if len(results.Errors) > 0 {
			for _, errMsg := range results.Errors {
				fmt.Println(errMsg)
			}

			// Kill and signal an error
			os.Exit(1)
		}

		return nil
	})
}

func gitlabClient() (*gitlab.Client, error) {
	localRepo, err := gitlab.LocalRepo()
	if err != nil {
		return nil, err
	}

	project, err := localRepo.DefaultProject()
	if err != nil {
		return nil, err
	}

	return gitlab.New(project.Host), nil
}
