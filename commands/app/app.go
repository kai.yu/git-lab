package app

import (
	"os"

	"gitlab.com/jramsay/git-lab/commands/root"
)

// Run the command.
func Run(version string) error {
	_, err := root.Cmd.Parse(os.Args[1:])
	return err
}
