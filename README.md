# git + lab

A utility to retrieve commit status for current commit and more!

## Usage

```bash
$ git lab
```

## Configuration

Lab is configured by a `~/.gitlab` TOML file located in your home directory.


```toml
[[server]]
host = "gitlab.com"
personal_access_token = "abc"

[[server]]
host = "gitlab.example.com"
personal_access_token = "xyz"
```

## TODO

- write tests
- installation instructions
- example bash and zsh integration
