package gitlab

import (
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/jramsay/git-lab/config"

	g "github.com/jamesramsay/go-gitlab"
)

var UserAgent = "jramsay/Lab 0.0.1"

type Host struct {
	Host        string
	AccessToken string
}

type Client struct {
	Host *Host

	*g.Client
}

func NewClient(h string) (c *g.Client) {
	c = g.NewClient(nil, accessTokenForHost(h))
	c.SetBaseURL(fmt.Sprintf("http://%s/api/v4", h))

	return
}

func New(host string) *Client {
	h := &Host{Host: host, AccessToken: accessTokenForHost(host)}
	return &Client{h, g.NewClient(nil, h.AccessToken)}
}

func accessTokenForHost(h string) string {
	for _, s := range config.Config.Servers {
		if s.Host == h {
			return s.AccessToken
		}
	}

	return ""
}

func (client *Client) absolute(host string) *url.URL {
	u, _ := url.Parse("http://" + host + "/")
	return u
}

func (client *Client) apiClient() *simpleClient {
	httpClient := http.Client{}
	apiRoot := client.absolute(client.Host.Host)
	apiRoot.Path = "/api/v4/"

	c := &simpleClient{
		httpClient: &httpClient,
		rootUrl:    apiRoot,
	}

	c.PrepareRequest = func(req *http.Request) {
		req.Header.Set("Private-Token", client.Host.AccessToken)
		req.Header.Set("User-Agent", UserAgent)
	}

	return c
}
