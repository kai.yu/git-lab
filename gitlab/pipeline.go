package gitlab

import (
	"fmt"
	"strings"
	"time"

	g "github.com/jamesramsay/go-gitlab"
)

type Job struct {
	Id         int
	Name       string
	Stage      string
	Status     string
	FinishedAt *time.Time
}

type PipelineStage struct {
	Name   string
	Status string
	Jobs   []*Job
}

type Pipeline struct {
	Id     int
	Status string
	URL    string
	Jobs   []*Job
	Stages []*PipelineStage
}

func FetchPipeline(c *g.Client, p *Project, id int) (*Pipeline, error) {
	// TODO: Pagination... only the first page of results is being used!
	opts := &g.ListJobsOptions{ListOptions: g.ListOptions{PerPage: 100}}
	jobs, _, err := c.Jobs.ListPipelineJobs(p.Path(), id, opts)
	if err != nil {
		return nil, err
	}

	pipeline := Pipeline{
		Id:     id,
		URL:    fmt.Sprintf("https://%s/%s/%s/pipelines/%d", p.Host, p.Namespace, p.Name, id),
		Stages: []*PipelineStage{},
		Jobs:   []*Job{},
	}

	stages := make(map[string]struct{})

	for _, j := range jobs {
		if j.Status == "manual" {
			continue
		}

		job := Job{
			Id:         j.ID,
			Name:       j.Name,
			Status:     j.Status,
			Stage:      j.Stage,
			FinishedAt: j.FinishedAt,
		}

		pipeline.Jobs = append(pipeline.Jobs, &job)
		stages[j.Stage] = struct{}{}
	}

	pipeline.Jobs = deduplicateJobs(pipeline.Jobs)

	pipeline.Status = JobsStatus(pipeline.Jobs)

	for s := range stages {
		stage := PipelineStage{
			Name: s,
			Jobs: []*Job{},
		}

		pipeline.Stages = append(pipeline.Stages, &stage)

		for _, j := range pipeline.Jobs {
			if j.Stage == s {
				stage.Jobs = append(stage.Jobs, j)
			}
		}

		stage.Status = JobsStatus(stage.Jobs)
	}

	return &pipeline, nil
}

func FetchLastPipelineByCommit(c *g.Client, p *Project, sha string, ref string) (*Pipeline, error) {
	// TODO: Pagination... only the first page of results is being used!
	optsPipeline := &g.ListProjectPipelinesOptions{Ref: &ref, ListOptions: g.ListOptions{PerPage: 100}}
	pipelines, _, err := c.Pipelines.ListProjectPipelines(p.Path(), optsPipeline)
	if err != nil {
		return nil, err
	}

	// TODO: remove once https://gitlab.com/gitlab-org/gitlab-ce/issues/44880 is accepted
	pipelineId := pipelineIdFromSha(pipelines, sha)
	if pipelineId == 0 {
		return nil, fmt.Errorf("Could not find matching pipeline")
	}

	pipeline, err := FetchPipeline(c, p, pipelineId)
	return pipeline, err
}

func JobsStatus(jobs []*Job) (s string) {
	var count = map[string]int{}

	s = ""

	for _, j := range jobs {
		if j.Status == "failed" {
			s = j.Status
			return
		}

		count[j.Status]++
	}

	n := len(jobs) - count["manual"]

	switch n {
	case 0:
		return
	case count["pending"]:
		s = "pending"
	case count["cancelled"]:
		s = "cancelled"
	case count["failed"]:
		s = "failed"
	case count["success"]:
		s = "success"
	default:
		s = "running"
	}

	return
}

func (s *PipelineStage) StatusDescription() string {
	var counts = map[string]int{}

	for _, j := range s.Jobs {
		counts[j.Status]++
	}

	parts := []string{}
	for status, count := range counts {
		parts = append(parts, fmt.Sprintf("%d %s", count, status))
	}

	return strings.Join(parts, ", ")
}

func (p *Pipeline) FindJob(name string) *Job {
	return findJob(p.Jobs, name)
}

func findJob(jobs []*Job, name string) *Job {
	for _, j := range jobs {
		if j.Name == name {
			return j
		}
	}
	return nil
}

func deduplicateJobs(jobs []*Job) []*Job {
	distinctJobs := make(map[string]*Job)

	for _, j := range jobs {
		if distinctJobs[j.Name] != nil {
			aFinishedAt := distinctJobs[j.Name].FinishedAt
			bFinishedAt := j.FinishedAt

			if aFinishedAt.After(*bFinishedAt) {
				continue
			}
		}
		distinctJobs[j.Name] = j
	}

	deduped := []*Job{}
	for _, j := range distinctJobs {
		deduped = append(deduped, j)
	}

	return deduped
}

func SummarizeJobsByStage(jobs []*g.Job) map[string]string {
	var mapByStage = map[string][]*g.Job{}

	for _, j := range jobs {
		mapByStage[j.Stage] = append(mapByStage[j.Stage], j)
	}

	var s = make(map[string]string)
	for k, j := range mapByStage {
		s[k] = summarizeJobs(j)
	}

	return s
}

func summarizeJobs(jobs []*g.Job) (s string) {
	var count = map[string]int{}

	s = ""

	for _, j := range jobs {
		if j.Status == "failed" {
			s = j.Status
			return
		}

		count[j.Status]++
	}

	n := len(jobs) - count["manual"]

	switch n {
	case 0:
		return
	case count["pending"]:
		s = "pending"
	case count["cancelled"]:
		s = "cancelled"
	case count["failed"]:
		s = "failed"
	case count["success"]:
		s = "success"
	default:
		s = "running"
	}

	return
}

func pipelineIdFromSha(pipelines g.PipelineList, sha string) int {
	for _, p := range pipelines {
		if p.Sha == sha {
			return p.ID
		}
	}
	return 0
}
