package gitlab

import (
	"fmt"
	"net/url"
	"regexp"
	"strings"
)

type Project struct {
	Name      string
	Namespace string
	Host      string
}

var (
	patternUrl    = regexp.MustCompile(`^(?P<protocol>https://|http://|ssh://)(?:(?P<user>.+)(?:@))(?P<host>.+?)(?::(?P<port>\d+)){0,1}/(?P<project>.+)\.git$`)
	patternScpUrl = regexp.MustCompile(`^(?:(?P<user>.+)(?:@))(?P<host>.+?):(?P<project>.+)\.git$`)
)

func (p *Project) Path() string {
	return fmt.Sprintf("%s/%s", p.Namespace, p.Name)
}

func NewProjectFromURL(url *url.URL) (p *Project, err error) {
	parts := strings.Split(url.Path, "/")

	if len(parts) <= 2 {
		err = fmt.Errorf("Invalid GitLab URL: %s", url)
		return
	}

	name := strings.TrimSuffix(parts[len(parts)-1], ".git")
	namespace := strings.Join(parts[1:len(parts)-1], "/")

	p = &Project{
		Name:      name,
		Namespace: namespace,
		Host:      url.Host,
	}

	return
}

// TODO: move to utils
func FindStringSubmatchMap(r *regexp.Regexp, s string) map[string]string {
	captures := make(map[string]string)

	match := r.FindStringSubmatch(s)
	if match == nil {
		return captures
	}

	for i, name := range r.SubexpNames() {
		// Ignore the whole regexp match and unnamed groups
		if i == 0 || name == "" {
			continue
		}

		captures[name] = match[i]

	}
	return captures
}
